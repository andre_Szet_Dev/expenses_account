<?php
namespace App\Form\Back\Expenses;

use App\Entity\Expense\ExpensesAccount;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddExpensesAccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label'=>'Nom',
                'attr' => ['readonly'=> true, 'value'=>"EA-"

                ]
            ))

            ->add('start', DateTimeType::class, array(
                'label'=>'Début',
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/mm/YYYY',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                ]
            ))
            ->add('end', DateType::class, array(
                'label'=>'Début',
                'required' => true,
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/mm/YYYY',
                'attr' => [
                    'class' => 'form-control input-inline datepicker',
                    'data-provide' => 'datepicker',
                ],
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ExpensesAccount::class,
        ]);
    }
}