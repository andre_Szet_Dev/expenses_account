<?php


namespace App\Form\Back\Expenses;


use App\Entity\Expense\Currency;
use App\Entity\Expense\ExpenseType;
use App\Entity\Expense\Vehicle;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AddLineExpenseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options); // TODO: Change the autogenerated stub

        $builder
            ->add ('description', TextType::class, array(
                'label'=>'Description',
                'required'=>true,
                "help"=> "Bref descriptif du motif de la dépense"
            ))
            ->add('establishment', TextType::class, array(
               'label'=> 'Établissement',
               'help'=>'Nom de la société qui a établi la facture'
            ))
            ->add('billing_date', DateType::class, array(
                'label'=> 'Date Facture',
                'widget'=>'single_text',
                'input'=>'datetime_immutable',
                'required'=>true,
                'help'=> 'Date inscrite sur la facture',
                'attr'=>["class"=>"js-datepicker"],
                "html5"=>false
            ))
            ->add('type', EntityType::class, array(
                'label'=> 'Type Facture',
                'required'=>true,
                'class' => ExpenseType::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->where('t.isActive = 1')
                        ->orderBy('t.name');
                },
                'choice_label' => 'name',
                'expanded' => false,
                'multiple' => false
            ))
            ->add('billing_amount', MoneyType::class, array(
                'label'=>'Montant TTC',
                'required'=>false,
                'html5'=>true,
                'attr'=>['min'=>0]
            ))
            ->add('currency', EntityType::class, array(
                'label'=>'Devise',
                'required'=>true,
                'class'=>Currency::class,
                    'query_builder'=>function(EntityRepository $er){
                    return $er->createQueryBuilder('c')
                        ->where('c.isActive =1')
                        ->orderBy('c.id');
                    },
                    'choice_label'=>'name',
                    'choice_value'=>'id',
                    'expanded'=>false,
                    'multiple'=>false
            ))
            ->add('departure', TextType::class, array(
                'label'=>'Adresse de départ',
                'help'=>'Rue, Code Postale, Ville'
            ))
            ->add('arrival', TextType::class, array(
                'label'=>'Adresse d\'arrivée',
                'help'=> 'Rue, Code Postale, Ville',
            ))
            ->add('kilometers', NumberType::class, array(
                'label'=> 'Nombre de kilomètres',
                'html5'=> true,
                'attr'=>['min'=>0]
            ))
            ->add('vehicle', EntityType::class, array(
                'label'=>'Véhicule utilisé',
                'class'=>Vehicle::class,
                'query_builder'=>function(EntityRepository $er){
                    return $er->createQueryBuilder('c')
                        ->where('c.isActive =1')
                        ->orderBy('c.id');
                },
                'choice_label'=>'brands',
                'choice_value'=>'id',
                'expanded'=>false,
                'multiple'=>false
            ))
            ->add('total_amount', MoneyType::class, array(
                'label'=>'Montant Total TTC',
                'required'=>true,
                'attr'=>['readonly'=>true]
            ))
            ->add('partner', TextType::class, array(
                'label'=>'Client',
                'help'=> 'Nom du client concerné par la note',
                'required'=>false
            ))
            ->add('comment', TextareaType::class, array(
                'label'=> 'Commentaire',
                'attr'=>['class'=>'ckeditor']
            ))
            ->add('start', DateType::class, array(
                'label'=> 'Date de départ',
                'widget'=>'single_text',
                'input'=>'datetime_immutable',
                'required'=>false,
                'help'=> 'Date de départ du déplacement'
            ))
            ->add('end', DateType::class, array(
                'label'=> 'Date de retour',
                'widget'=>'single_text',
                'input'=>'datetime_immutable',
                'required'=>false,
                'help'=> 'Date de retour du déplacement'
            ))
        ;


    }
}