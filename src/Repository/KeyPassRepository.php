<?php

namespace App\Repository\Security\Back;

use App\Entity\Security\Back\BackUser;
use App\Entity\Security\Back\KeyPass;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method KeyPass|null find($id, $lockMode = null, $lockVersion = null)
 * @method KeyPass|null findOneBy(array $criteria, array $orderBy = null)
 * @method KeyPass[]    findAll()
 * @method KeyPass[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KeyPassRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, KeyPass::class);
    }

    public function getListKeyPass(BackUser $backUser)
    {
        $qb = $this->createQueryBuilder("k")
            ->leftJoin('k.owner', 'owner' )
            ->leftJoin('k.sharedWith', 'shared_with')
            ->where('k.isActive = 1')
            ->andWhere('owner.id = :back_user')->setParameter('back_user', $backUser->getId())
            ->orWhere('shared_with.id = :back_user AND k.isActive = 1')
            ->orWhere('k.isPrivate = false AND k.isActive = 1')
        ;

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return KeyPass[] Returns an array of KeyPass objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?KeyPass
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
