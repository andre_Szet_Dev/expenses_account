<?php

namespace App\Repository\Expense;

use App\Entity\Expense\ExpensesAccount;
use App\Entity\Expense\ExpensesAccountStatus;
use App\Entity\Security\Back\BackUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExpensesAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExpensesAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExpensesAccount[]    findAll()
 * @method ExpensesAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpensesAccountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExpensesAccount::class);
    }

    public function getListExpensesAccounts(BackUser $backUser){
        $qb = $this->createQueryBuilder('e')
            ->leftJoin('e.createdBy', 'creator')
            ->leftJoin('e.status','status')
            ->where('e.isActive =1')
            ->andWhere('creator.id = :back_user')->setParameter('back_user', $backUser->getId())
        ;

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return ExpensesAccount[] Returns an array of ExpensesAccount objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExpensesAccount
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
