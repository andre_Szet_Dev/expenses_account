<?php

namespace App\Repository\Expense;

use App\Entity\Expense\ExpensesAccountStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExpensesAccountStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExpensesAccountStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExpensesAccountStatus[]    findAll()
 * @method ExpensesAccountStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpensesAccountStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExpensesAccountStatus::class);
    }

    // /**
    //  * @return ExpensesAccountStatus[] Returns an array of ExpensesAccountStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExpensesAccountStatus
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
