<?php

namespace App\Repository\Expense;

use App\Entity\Expense\ExpensesAccount;
use App\Entity\Expense\ExpensesAccountLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ExpensesAccountLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExpensesAccountLine|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExpensesAccountLine[]    findAll()
 * @method ExpensesAccountLine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpensesAccountLineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExpensesAccountLine::class);
    }

    public function getListLinesExpenses(ExpensesAccount $expensesAccount){
        $qb = $this->createQueryBuilder('e')
            ->leftJoin('e.account', 'account')
            ->leftJoin('e.currency','currency')
            ->leftJoin('e.vehicle', 'vehicle')
            ->where('e.isActive =1')
            ->andWhere('account.id = :account')->setParameter('account', $expensesAccount->getId())
        ;

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return ExpensesAccountLine[] Returns an array of ExpensesAccountLine objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExpensesAccountLine
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
