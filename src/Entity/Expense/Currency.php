<?php

namespace App\Entity\Expense;

use App\Repository\Expense\CurrencyRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CurrencyRepository::class)
 */
class Currency
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $code_currency;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $exchange_rate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity=ExpensesAccountLine::class, mappedBy="currency")
     */
    private $expensesAccountLines;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $symbole;

    public function __construct()
    {
        $this->expensesAccountLine = new ArrayCollection();
        $this->expensesAccountLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCodeCurrency(): ?string
    {
        return $this->code_currency;
    }

    public function setCodeCurrency(?string $code_currency): self
    {
        $this->code_currency = $code_currency;

        return $this;
    }

    public function getExchangeRate(): ?float
    {
        return $this->exchange_rate;
    }

    public function setExchangeRate(?float $exchange_rate): self
    {
        $this->exchange_rate = $exchange_rate;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|ExpensesAccountLine[]
     */
    public function getLineExpensesAccounts(): Collection
    {
        return $this->expensesAccountLine;
    }

    public function addLineExpensesAccount(ExpensesAccountLine $expensesAccountLine): self
    {
        if (!$this->expensesAccountLine->contains($expensesAccountLine)) {
            $this->expensesAccountLine[] = $expensesAccountLine;
            $expensesAccountLine->setCurrency($this);
        }

        return $this;
    }

    public function removeLineExpensesAccount(ExpensesAccountLine $expensesAccountLine): self
    {
        if ($this->expensesAccountLine->removeElement($expensesAccountLine)) {
            // set the owning side to null (unless already changed)
            if ($expensesAccountLine->getCurrency() === $this) {
                $expensesAccountLine->setCurrency(null);
            }
        }

        return $this;
    }

    public function getSymbole(): ?string
    {
        return $this->symbole;
    }

    public function setSymbole(?string $symbole): self
    {
        $this->symbole = $symbole;

        return $this;
    }

    /**
     * @return Collection|ExpensesAccountLine[]
     */
    public function getExpensesAccountLines(): Collection
    {
        return $this->expensesAccountLines;
    }

    public function addExpensesAccountLine(ExpensesAccountLine $expensesAccountLine): self
    {
        if (!$this->expensesAccountLines->contains($expensesAccountLine)) {
            $this->expensesAccountLines[] = $expensesAccountLine;
            $expensesAccountLine->setCurrency($this);
        }

        return $this;
    }

    public function removeExpensesAccountLine(ExpensesAccountLine $expensesAccountLine): self
    {
        if ($this->expensesAccountLines->removeElement($expensesAccountLine)) {
            // set the owning side to null (unless already changed)
            if ($expensesAccountLine->getCurrency() === $this) {
                $expensesAccountLine->setCurrency(null);
            }
        }

        return $this;
    }
}
