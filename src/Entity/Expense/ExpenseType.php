<?php

namespace App\Entity\Expense;

use App\Repository\Expense\ExpenseTypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExpenseTypeRepository::class)
 */
class ExpenseType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $code_type;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity=ExpensesAccountLine::class, mappedBy="type")
     */
    private $expensesAccountLines;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $vat;

    public function __construct()
    {
        $this->expensesAccountLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCodeType(): ?string
    {
        return $this->code_type;
    }

    public function setCodeType(?string $code_type): self
    {
        $this->code_type = $code_type;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getVat(): ?float
    {
        return $this->vat;
    }

    public function setVat(?float $vat): self
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * @return Collection|ExpensesAccountLine[]
     */
    public function getExpensesAccountLines(): Collection
    {
        return $this->expensesAccountLines;
    }

    public function addExpensesAccountLine(ExpensesAccountLine $expensesAccountLine): self
    {
        if (!$this->expensesAccountLines->contains($expensesAccountLine)) {
            $this->expensesAccountLines[] = $expensesAccountLine;
            $expensesAccountLine->setType($this);
        }

        return $this;
    }

    public function removeExpensesAccountLine(ExpensesAccountLine $expensesAccountLine): self
    {
        if ($this->expensesAccountLines->removeElement($expensesAccountLine)) {
            // set the owning side to null (unless already changed)
            if ($expensesAccountLine->getType() === $this) {
                $expensesAccountLine->setType(null);
            }
        }

        return $this;
    }
}
