<?php

namespace App\Entity\Expense;

use App\Repository\Expense\ExpensesAccountStatusRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExpensesAccountStatusRepository::class)
 */
class ExpensesAccountStatus
{
    const EXPENSES_ACCOUNT_STATUS_CODE_SUBMITTED = "submitted";
    const EXPENSES_ACCOUNT_STATUS_CODE_IN_PROGRESS = "in_progress";
    const EXPENSES_ACCOUNT_STATUS_CODE_ACCEPTED = "accepted";
    const EXPENSES_ACCOUNT_STATUS_CODE_REJECTED = "rejected";
    const EXPENSES_ACCOUNT_STATUS_CODE_ARCHIVED = "archived";

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity=ExpensesAccount::class, mappedBy="status")
     */
    private $expensesAccounts;

    public function __construct()
    {
        $this->expensesAccounts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|ExpensesAccount[]
     */
    public function getExpensesAccounts(): Collection
    {
        return $this->expensesAccounts;
    }

    public function addExpensesAccount(ExpensesAccount $expensesAccount): self
    {
        if (!$this->expensesAccounts->contains($expensesAccount)) {
            $this->expensesAccounts[] = $expensesAccount;
            $expensesAccount->setStatut($this);
        }

        return $this;
    }

    public function removeExpensesAccount(ExpensesAccount $expensesAccount): self
    {
        if ($this->expensesAccounts->removeElement($expensesAccount)) {
            // set the owning side to null (unless already changed)
            if ($expensesAccount->getStatut() === $this) {
                $expensesAccount->setStatut(null);
            }
        }

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }
}
