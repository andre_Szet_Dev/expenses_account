<?php

namespace App\Entity\Expense;

use App\Repository\Expense\VehicleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VehicleRepository::class)
 */
class Vehicle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $registration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $brands;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $horsepower_tax;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $kilometer_allowance;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $professionnal;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity=ExpensesAccountLine::class, mappedBy="vehicle")
     */
    private $expensesAccountLines;

    public function __construct()
    {
        $this->expensesAccountLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRegistration(): ?string
    {
        return $this->registration;
    }

    public function setRegistration(string $registration): self
    {
        $this->registration = $registration;

        return $this;
    }

    public function getBrands(): ?string
    {
        return $this->brands;
    }

    public function setBrands(?string $brands): self
    {
        $this->brands = $brands;

        return $this;
    }

    public function getHorsepowerTax(): ?int
    {
        return $this->horsepower_tax;
    }

    public function setHorsepowerTax(?int $horsepower_tax): self
    {
        $this->horsepower_tax = $horsepower_tax;

        return $this;
    }

    public function getKilometerAllowance(): ?float
    {
        return $this->kilometer_allowance;
    }

    public function setKilometerAllowance(?float $kilometer_allowance): self
    {
        $this->kilometer_allowance = $kilometer_allowance;

        return $this;
    }

    public function getProfessionnal(): ?bool
    {
        return $this->professionnal;
    }

    public function setProfessionnal(?bool $professionnal): self
    {
        $this->professionnal = $professionnal;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|ExpensesAccountLine[]
     */
    public function getLineExpensesAccounts(): Collection
    {
        return $this->expensesAccountLines;
    }

    public function addLineExpensesAccount(ExpensesAccountLine $expensesAccountLines): self
    {
        if (!$this->expensesAccountLines->contains($expensesAccountLines)) {
            $this->expensesAccountLines[] = $expensesAccountLines;
            $expensesAccountLines->setVehicle($this);
        }

        return $this;
    }

    public function removeLineExpensesAccount(ExpensesAccountLine $expensesAccountLines): self
    {
        if ($this->expensesAccountLines->removeElement($expensesAccountLines)) {
            // set the owning side to null (unless already changed)
            if ($expensesAccountLines->getVehicle() === $this) {
                $expensesAccountLines->setVehicle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ExpensesAccountLine[]
     */
    public function getExpensesAccountLines(): Collection
    {
        return $this->expensesAccountLines;
    }

    public function addExpensesAccountLine(ExpensesAccountLine $expensesAccountLine): self
    {
        if (!$this->expensesAccountLines->contains($expensesAccountLine)) {
            $this->expensesAccountLines[] = $expensesAccountLine;
            $expensesAccountLine->setVehicle($this);
        }

        return $this;
    }

    public function removeExpensesAccountLine(ExpensesAccountLine $expensesAccountLine): self
    {
        if ($this->expensesAccountLines->removeElement($expensesAccountLine)) {
            // set the owning side to null (unless already changed)
            if ($expensesAccountLine->getVehicle() === $this) {
                $expensesAccountLine->setVehicle(null);
            }
        }

        return $this;
    }
}
