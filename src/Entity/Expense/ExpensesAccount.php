<?php

namespace App\Entity\Expense;

use App\Entity\Security\Back\BackUser;
use App\Repository\Expense\ExpensesAccountRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExpensesAccountRepository::class)
 */
class ExpensesAccount
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $start;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $end;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPaid;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_payment;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $repayment_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sum_owned;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\ManyToOne(targetEntity=BackUser::class, inversedBy="expensesAccounts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity=MethodPayment::class, inversedBy="expensesAccounts")
     */
    private $method_payment_by;

    /**
     * @ORM\ManyToOne(targetEntity=ExpensesAccountStatus::class, inversedBy="expensesAccounts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity=ExpensesAccountLine::class, mappedBy="account")
     */
    private $expensesAccountLines;

    public function __construct(ExpensesAccountStatus $initStatus)
    {
        $this->expensesAccountLines = new ArrayCollection();
        $this->isPaid = false;
        $this->isActive = 1;
        $this->status = $initStatus;

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStart(): ?DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(?DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getIsPaid(): ?bool
    {
        return $this->isPaid;
    }

    public function setIsPaid(bool $isPaid): self
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    public function getDatePayment(): ?DateTimeInterface
    {
        return $this->date_payment;
    }

    public function setDatePayment(?DateTimeInterface $date_payment): self
    {
        $this->date_payment = $date_payment;

        return $this;
    }

    public function getRepaymentNumber(): ?int
    {
        return $this->repayment_number;
    }

    public function setRepaymentNumber(?int $repayment_number): self
    {
        $this->repayment_number = $repayment_number;

        return $this;
    }

    public function getSumOwned(): ?string
    {
        return $this->sum_owned;
    }

    public function setSumOwned(?string $sum_owned): self
    {
        $this->sum_owned = $sum_owned;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getCreatedBy(): ?BackUser
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?BackUser $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getMethodPaymentBy(): ?MethodPayment
    {
        return $this->method_payment_by;
    }

    public function setMethodPaymentBy(?MethodPayment $method_payment_by): self
    {
        $this->method_payment_by = $method_payment_by;

        return $this;
    }

    public function getStatus(): ?ExpensesAccountStatus
    {
        return $this->status;
    }

    public function setStatus(?ExpensesAccountStatus $status): self
    {
        $this->status= $status;

        return $this;
    }

    /**
     * @return Collection|ExpensesAccountLine[]
     */
    public function getLineExpensesAccounts(): Collection
    {
        return $this->expensesAccountLines;
    }

    public function addLineExpensesAccount(ExpensesAccountLine $expensesAccountLines): self
    {
        if (!$this->expensesAccountLines->contains($expensesAccountLines)) {
            $this->expensesAccountLines[] = $expensesAccountLines;
            $expensesAccountLines->setAccount($this);
        }

        return $this;
    }

    public function removeLineExpensesAccount(ExpensesAccountLine $expensesAccountLines): self
    {
        if ($this->expensesAccountLines->removeElement($expensesAccountLines)) {
            // set the owning side to null (unless already changed)
            if ($expensesAccountLines->getAccount() === $this) {
                $expensesAccountLines->setAccount(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ExpensesAccountLine[]
     */
    public function getExpensesAccountLines(): Collection
    {
        return $this->expensesAccountLines;
    }

    public function addExpensesAccountLine(ExpensesAccountLine $expensesAccountLine): self
    {
        if (!$this->expensesAccountLines->contains($expensesAccountLine)) {
            $this->expensesAccountLines[] = $expensesAccountLine;
            $expensesAccountLine->setAccount($this);
        }

        return $this;
    }

    public function removeExpensesAccountLine(ExpensesAccountLine $expensesAccountLine): self
    {
        if ($this->expensesAccountLines->removeElement($expensesAccountLine)) {
            // set the owning side to null (unless already changed)
            if ($expensesAccountLine->getAccount() === $this) {
                $expensesAccountLine->setAccount(null);
            }
        }

        return $this;
    }


}
