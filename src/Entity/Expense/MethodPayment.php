<?php

namespace App\Entity\Expense;

use App\Repository\Expense\MethodPaymentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MethodPaymentRepository::class)
 */
class MethodPayment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $code_method;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity=ExpensesAccount::class, mappedBy="method_payment_by")
     */
    private $expensesAccounts;

    public function __construct()
    {
        $this->expensesAccounts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCodeMethod(): ?string
    {
        return $this->code_method;
    }

    public function setCodeMethod(?string $code_method): self
    {
        $this->code_method = $code_method;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeInterface
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeInterface $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection|ExpensesAccount[]
     */
    public function getExpensesAccounts(): Collection
    {
        return $this->expensesAccounts;
    }

    public function addExpensesAccount(ExpensesAccount $expensesAccount): self
    {
        if (!$this->expensesAccounts->contains($expensesAccount)) {
            $this->expensesAccounts[] = $expensesAccount;
            $expensesAccount->setMethodPaymentBy($this);
        }

        return $this;
    }

    public function removeExpensesAccount(ExpensesAccount $expensesAccount): self
    {
        if ($this->expensesAccounts->removeElement($expensesAccount)) {
            // set the owning side to null (unless already changed)
            if ($expensesAccount->getMethodPaymentBy() === $this) {
                $expensesAccount->setMethodPaymentBy(null);
            }
        }

        return $this;
    }
}
