<?php


namespace App\Services\KeyPass;


use App\Entity\Security\Back\KeyPass;

interface KeyPassInterface
{
    public function upsert(KeyPass $keyPass) : KeyPass;
    public function displayPassword(KeyPass $keyPass):string;
}