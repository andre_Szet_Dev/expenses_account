<?php


namespace App\Controller\Back\Settings;


use App\Entity\Expense\Currency;
use App\Entity\Expense\ExpensesAccountLine;
use App\Entity\Expense\ExpensesAccountStatus;
use App\Entity\Expense\ExpenseType;
use App\Entity\Expense\Vehicle;
use App\Form\Back\Expenses\AddExpensesAccountType;
use App\Entity\Expense\ExpensesAccount;
use App\Form\Back\Expenses\AddLineExpenseType;
use App\Services\Expense\ExpenseInterface;
use App\Services\Expense\ExpenseManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ExpensesAccountController
 * @package App\Controller\Back\Settings
 * @Route ("/expense/account")
 */
class ExpensesAccountController extends AbstractController
{
    /**
     * @Route("/", name="back_expenses_account_index")
     */
    public function listExpensesAccount(Request $request, ExpenseInterface $expenseManager):Response
    {
        $em = $this->getDoctrine()->getManager();
        $initStatus = $em->getRepository(ExpensesAccountStatus::class)->findOneBy(array('code'=> ExpensesAccountStatus::EXPENSES_ACCOUNT_STATUS_CODE_IN_PROGRESS));
        //if (!$initStatus instanceof ExpensesAccountStatus) throw new Exception('ExpenseStatus : Unable to find in_progress status in BDD');
        $new_expenses_account = new ExpensesAccount($initStatus);
        $formAddExpensesAccount = $this->createForm(AddExpensesAccountType::class, $new_expenses_account);
        $formAddExpensesAccount ->handleRequest($request);

        if ($formAddExpensesAccount->isSubmitted() && $formAddExpensesAccount->isValid())
        {
            $expenseManager->upsert($new_expenses_account);
            $param_render = array (
                'expenseAccount'=>$new_expenses_account
            );
            return $this->render('Back/Expense/listLineExpenses.html.twig', $param_render);
        }

        $expenses_accounts_em = $em->getRepository(ExpensesAccount::class)->getListExpensesAccounts($this->getUser());
        $expenses_accounts = $expenses_accounts_history= array();

        foreach ($expenses_accounts_em as $expense_account)
        {
            if ($expense_account->getStatus() &&
                $expense_account->getStatus()->getCode() ===
                ExpensesAccountStatus::EXPENSES_ACCOUNT_STATUS_CODE_ARCHIVED)
            {
                $expenses_accounts_history[] = $expense_account;
            } else {
                $expenses_accounts[] = $expense_account;
            }
        }

        $param_render=array(
            'expensesAccounts'=>$expenses_accounts,
            'historyExpensesAccounts'=>$expenses_accounts_history,
            'formAddExpensesAccount'=>$formAddExpensesAccount->createView()
        );

        return $this->render('Back/Expense/listExpensesAccount.html.twig', $param_render);
    }

    /**
     * @Route("/view/{id}", name="back_line_expenses_view")
     */
    public function backLineExpensesView(ExpensesAccount $expensesAccount)
    {
        $param_render = array (
            'expenseAccount'=>$expensesAccount
        );
        return $this->render('Back/Expense/listLineExpenses.html.twig', $param_render);
    }

    /**
     * @Route("/add/expense/{id}", name="back_add_line_expense")
     */
    public function addLineExpense(Request $request, ExpensesAccount $expensesAccount, ExpenseManager $expenseManager):Response
    {
        $new_expense_line = new ExpensesAccountLine();
        $formAddLineExpense = $this->createForm(AddLineExpenseType::class, $new_expense_line);
        $formAddLineExpense ->handleRequest($request);

        if($formAddLineExpense->isSubmitted()&& $formAddLineExpense->isValid())
        {
            $expenseManager->upsertLine($expensesAccount, $new_expense_line);

            return $this->redirectToRoute('back_line_expenses_view', array('id'=>$expensesAccount->getId()));
        }

        $param_render = array(
            'expenseAccount'=>$expensesAccount,
            'formAddLineExpense'=>$formAddLineExpense->createView()
        );

        return $this->render('Back/Expense/addLineExpenses.html.twig', $param_render);
    }

    /**
     * @Route ("/search/currency/{id}", name="back_search_currency")
     */

    public function searchCurrency(Currency $currency):JsonResponse
    {
        $result=[
            'name'=>$currency->getName(),
            'code'=>$currency->getCodeCurrency(),
            'exchange_rate'=>$currency->getExchangeRate(),
            'symbole'=>$currency->getSymbole()

        ];

        return $this->json($result) ;
    }
    /**
     * @Route ("/search/expense/type/{id}", name="back_search_expense_type")
     */

    public function searchExpenseType(ExpenseType $type):JsonResponse
    {
        $result=[
            'name'=>$type->getName(),
            'code'=>$type->getCodeType(),
        ];

        return $this->json($result) ;
    }

    /**
     * @Route ("/search/vehicle/{id}", name="back_search_vehicle")
     */
    public function searchVehicle(Vehicle $vehicle):JsonResponse
    {
        $result=[
            'allowance'=>$vehicle->getKilometerAllowance(),
        ];

        return $this->json($result) ;
    }

/*    /**
     * Route ("display/edit/expense/{id}", name="back_display_edit_expense")
     */

//  */  public function displayEditExpense(Request $request, ExpensesAccountLine $expense)

}